from PIL import ImageEnhance, Image

file = 'input/venice.jpg'

# seconds
seconds = 5

fps = 24
extra_duration = 0 * fps

total_frames = seconds * fps
color_percentage_for_each_frame = (100 / total_frames) / 100  # for the 0. mark

im = Image.open(file)

images = []
for i in range(total_frames):
    processed = ImageEnhance.Color(im).enhance(color_percentage_for_each_frame * i)
    images.append(processed)
print(images)
