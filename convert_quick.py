import imageio
from PIL import ImageEnhance, Image

images = []
file = 'input/venice.jpg'

# seconds
seconds = 1

fps = 15
extra_duration = 3 * fps

total_frames = seconds * fps
color_percentage_for_each_frame = (100 / total_frames) / 100  # for the 0. mark

im = Image.open(file)

for i in range(total_frames):
    processed = ImageEnhance.Color(im).enhance(color_percentage_for_each_frame * i)
    images.append(processed)

    # images.append(imageio.imread(filename))
imageio.mimsave('output/new.gif', images)