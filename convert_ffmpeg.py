from PIL import ImageEnhance, Image
from subprocess import Popen, PIPE, call

file = 'test.jpg'

# secondsa
seconds = 5

fps = 15
extra_duration = 3 * fps

total_frames = seconds * fps
color_percentage_for_each_frame = (100 / total_frames) / 100  # for the 0. mark

im = Image.open(file)

save_path = 'processed'

p = Popen(['ffmpeg', '-y', '-f', 'image2pipe', '-vcodec', 'mjpeg', '-r', '15', '-i', '-', '-vcodec', 'mjpeg', '-t', '5', '-r', '15', '-y', 'output/movie1st.mp4'], stdin=PIPE)

for i in range(total_frames):
    if i < total_frames:
        processed = ImageEnhance.Color(im).enhance(color_percentage_for_each_frame * i)
        processed.save(p.stdin, 'JPEG')
    else:
        im.save(p.stdin, 'JPEG')
p.stdin.close()
p.wait()

call(['ffmpeg', '-t', '5', '-f', 'image2', '-r', '1/5', '-i', 'input/venice.jpg', '-vcodec', 'libx264', '-y', 'output/movie2nd.mp4'])